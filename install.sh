#!/bin/bash
cd "$(dirname "$0")"
SCRIPTPATH="$(pwd -P )"

for file in .*; do
  if [[ ${file} == '.' || ${file} == '..' ]]; then continue; fi

  homefile=${HOME}'/'${file}
  
  if [[ -e ${homefile} ]]; then
    echo "Backing up ${homefile} to ${homefile}.orig."
    mv ${homefile} ${homefile}.orig
  fi

  ln -s ${SCRIPTPATH}/${file} ${HOME}/${file}

done
