Suggested use:

Clone or download to local machine into a folder that is NOT home (home as a repository is wicked fun, but requires much aspirin.)
Use install.sh to create symlinks to these dotfiles.

To Install:
bash install.sh

To Uninstall:
bash uninstall.sh


Yes, it's that easy.
No, it won't delete your own precious dotfiles. (It actually backs them up as <filename>.orig, SUPAH FURENDURI!)
Yes, you are welcome to modify as you desire, even merge your own dotfiles with these. Heck, I'll even consider perusing pull requests.
